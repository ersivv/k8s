Добавляем метки к нодам 

```
kubectl label nodes k81ing1.ers.net.ru k81ing2.ers.net.ru node-role.kubernetes.io/ingress=
```

```
kubectl taint nodes k81ing1.ers.net.ru k81ing2.ers.net.ru IngressOnly=true:NoSchedule
```