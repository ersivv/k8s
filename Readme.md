
```mermaid
graph TD
    A[fa:fa-ethernet Mikrotik] --> |NAT 80:80 443:443| B(HAPROXY)
    B --> |NodePort 30080 30443| C(node-ingress)
    B --> |NodePort 30080 30443| D(node-ingress)

    C --> E(node-worker)
    C --> F(node-worker)
    C --> G(node-worker)

    D --> E(node-worker)
    D --> F(node-worker)
    D --> G(node-worker)
```